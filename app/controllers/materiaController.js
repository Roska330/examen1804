const Materia = require('../models/Materia')
const { ObjectId } = require('mongodb')

// Funciona
const index = (req, res) => {
    console.log("Llego a materias");
  Materia.find((err, materias) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista de materias'
      })
    }
    return res.json(materias)
  })
}
// No funciona
const indexPrivado = (req, res) => {
    console.log("Llego a materias");
  Materia.find((err, materias) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo  la lista de materias con token'
      })
    }
    return res.json(materias)
  })
}

// Funciona
const create = (req, res) => {
  const materia = new Materia(req.body)
  materia.save((err, materia) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar la materia',
        error: err
      })
    }
    return res.status(201).json(materia)
  })
}

// Funciona
const show = (req, res) => {
  const cod = req.params.id
  Materia.findById(cod, (err, materia) => {
    if (err) return res.status(500).json({ message: 'error' })
    if (!materia) return res.status(404).json({ message: 'not found' })
    return res.json(materia)
  })
}

// Funciona
const destroy = (req, res) => {
  const cod = req.params.id
  Materia.findByIdAndDelete(cod, (err, materia) => {
    if (err) return res.status(500).json({ message: 'error' })
    return res.json(materia)
  })
}

// Funciona
const update = (req, res) => {
  const cod = req.params.id
  Materia.findOne({ _id: cod }, (err, materia) => {
    if (!ObjectId.isValid(cod)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar la materia',
        error: err
      })
    }
    if (!materia) {
      return res.status(404).json({
        message: 'No hemos encontrado la materia'
      })
    }

    Object.assign(materia, req.body)

    materia.save((err, materia) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar la materia'
        })
      }
      if (!materia) {
        return res.status(404).json({
          message: 'No hemos encontrado la materia'
        })
      }
      return res.json(materia)
    })
  })
}

module.exports = {
  index,
  create,
  show,
  destroy,
  update,
  indexPrivado
}
