const User  = require('../models/User');
const servicejwt = require("../services/servicejwt")



const login = (req,res) => {
  console.log("Entro al login")
  let nom = req.body.nombre
  let password = req.body.password

  console.log(nom + " " + password)
  User.findOne({ nombre: nom }, (err, user) => {
    console.log(user)
    if(user != null) {
      return res.status(200).send({ token: servicejwt.createToken(user) })
    } else {
      return res.status(401).send("No existe el usuario")
    }
  })
}
module.exports = {
  login
}
