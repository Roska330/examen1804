const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MateriaSchema = new Schema({
  codigo: {
    type: String,
    required : true,
    unique: true    
  },
  nombre: String,
  curso: String,
  horas: Number
})
const Materia = mongoose.model('Materia', MateriaSchema)

module.exports = Materia
