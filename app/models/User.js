const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  codigo: {
    type: Number,
    required: true,
    unique: true    
  },
  nombre: String,
  password: String
})
const User = mongoose.model('User', UserSchema)

module.exports = User
